/*:
 * @plugindesc Tint screen when enemy is close
 * 
 * @author EdwardLi
 * 
 * @param light_switch_id
 * @desc The id of the lights_off switch
 * @default 2
 *
 * @help
 * Plugin command: chase [warning range]
 * when the player is within the warning range, the screen will start getting red.
 * when the event touches the player, the game ends.
 */


  (function() {
  	var switch_id = Number(PluginManager.parameters('plugin2')['light_switch_id']);
	Scene_Gameover.prototype.playGameoverMusic = function() {
	    AudioManager.stopBgm();
	    AudioManager.stopBgs();
	    //AudioManager.playSe($dataSystem.gameoverSe)
	    //AudioManager.playSe($dataSystem.gameoverSe);
	    AudioManager.playMe($dataSystem.gameoverMe);
	};

	Game_Interpreter.prototype.pluginCommand = function(command, args) {
	    if(command == 'chase'){

	    	var lightsoff = $gameSwitches.value(switch_id);
	    	console.log(lightsoff);
	    	var range = 7;
	    	if(args.length >=1 ){
	    		range = args[0];
	    	}
	    	var event = $gameMap.event(this._eventId);
	    	var e_x = event.x;
	    	var e_y = event.y;
	    	var p_x = $gamePlayer.x;
	    	var p_y = $gamePlayer.y;
	    	console.log(e_x +' '+ e_y);
	    	//console.log(p_x +' '+ p_y);
	    	var d_x = Math.abs(e_x - p_x);
	    	var d_y = Math.abs(e_y - p_y);
	    	var dist = parseInt(Math.sqrt(d_x*d_x + d_y*d_y));
	    	var red = $gameScreen.tone()[0];
	    	var green = $gameScreen.tone()[1];
	    	var blue = $gameScreen.tone()[2];
	    	//console.log('dist: '+ dist);
	    	//console.log('r: '+red+' b: '+blue+' g: '+green);
	    	if(dist <= range){
	    		var diff = range - dist;
	    		// var tint_d = (255)/range;
	    		// $gameScreen.startTint([diff*tint_d,green,blue],1);
	    		if(lightsoff){
	    			var tint_d = (255+68)/range;
	    			$gameScreen.startTint([-68+diff*tint_d,-68,-68],1);
	    		}
	    		else{
	    			var tint_d = 255/range;
	    			$gameScreen.startTint([diff*tint_d,0,0],1);

	    		}

	    	}
		    if($gamePlayer.pos(e_x,e_y)){
			    SceneManager.goto(Scene_Gameover);
			}

	    }
	};


 })();

