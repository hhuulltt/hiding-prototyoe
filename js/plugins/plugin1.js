/*:
 * @plugindesc Plugin for limiting menu items & adjusting menu position
 * 
 * @author EdwardLi
 *
 * @help
 * This plugin removes certain menu items(skill equip status formation)
 * Character status window and gold window are removed
 * Menu is now at the center of the screen
 */

 (function() {
 	Window_MenuCommand.prototype.makeCommandList = function() {
    	this.addMainCommands();
    	// this.addFormationCommand();
    	this.addOriginalCommands();
    	this.addOptionsCommand();
    	this.addSaveCommand();
    	this.addGameEndCommand();
	};
	Window_MenuCommand.prototype.addMainCommands = function() {
	    var enabled = this.areMainCommandsEnabled();
	    if (this.needsCommand('item')) {
	        this.addCommand(TextManager.item, 'item', enabled);
	    }
	    // if (this.needsCommand('skill')) {
	    //     this.addCommand(TextManager.skill, 'skill', enabled);
	    // }
	    // if (this.needsCommand('equip')) {
	    //     this.addCommand(TextManager.equip, 'equip', enabled);
	    // }
	    // if (this.needsCommand('status')) {
	    //     this.addCommand(TextManager.status, 'status', enabled);
	    // }
	};
	Scene_Menu.prototype.create = function() {
	    Scene_MenuBase.prototype.create.call(this);
	    this.createCommandWindow();
	    this.createGoldWindow();
	    this.createStatusWindow();
	};

	Scene_Menu.prototype.createCommandWindow = function() {
	    this._commandWindow = new Window_MenuCommand(0, 0);
	    this._commandWindow.x = Graphics.boxWidth/2 - this._commandWindow.width/2;
	    this._commandWindow.y = Graphics.boxHeight/2 - this._commandWindow.height/2;
	    this._commandWindow.setHandler('item',      this.commandItem.bind(this));
	    this._commandWindow.setHandler('skill',     this.commandPersonal.bind(this));
	    this._commandWindow.setHandler('equip',     this.commandPersonal.bind(this));
	    this._commandWindow.setHandler('status',    this.commandPersonal.bind(this));
	    this._commandWindow.setHandler('formation', this.commandFormation.bind(this));
	    this._commandWindow.setHandler('options',   this.commandOptions.bind(this));
	    this._commandWindow.setHandler('save',      this.commandSave.bind(this));
	    this._commandWindow.setHandler('gameEnd',   this.commandGameEnd.bind(this));
	    this._commandWindow.setHandler('cancel',    this.popScene.bind(this));
	    this.addWindow(this._commandWindow);
	};
	Scene_Menu.prototype.createGoldWindow = function() {
		this._goldWindow = new Window_Gold(0, 0);
    	this._goldWindow.y = Graphics.boxHeight - this._goldWindow.height;
		//this.addWindow(this._goldWindow);
	};

	Scene_Menu.prototype.createStatusWindow = function() {
		this._statusWindow = new Window_MenuStatus(this._commandWindow.width, 0);
    	this._statusWindow.reserveFaceImages();
    	//this.addWindow(this._statusWindow);
	};

 })();